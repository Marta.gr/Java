
public class Cuadrado extends Figura {
	
	//atributos de la clase Cuadrado
		// lado
	
	double lado;
	
	//Metodo constructor para crear objetos Cuadrado

	public Cuadrado(String color, char tamaño, double lado) {
		super(color, tamaño);
		this.lado = lado;
	}

	//Metodos get & set para obtener valor de atributo y para cambiar

	
	public double getLado() {
		return lado;
	}

	public void setLado(double lado) {
		this.lado = lado;
	}

	// Metodo toString 

	@Override
	public String toString() {
		return "Cuadrado [color=" + color + ", tamaño=" + tamaño + ", lado=" + lado + "]";
	}
	
	//Metodo calcularArea

		public double calcularArea() {
		
			
			return lado * lado;
			
		}
	

}
