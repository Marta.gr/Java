
public class Vehiculo {

	
	//Matricula, marca,	Modelo, Precio

		//atributos de la clase Vehiculo (padre)
			//Variables con su tipo
			String matricula;
			String marca;
			String modelo;
			double precio;
			
			
			//Metodo constructor para crear objetos vehiculo

			public Vehiculo(String matricula, String marca, String modelo, double precio) {
				super();
				this.matricula = matricula;
				this.marca = marca;
				this.modelo = modelo;
				this.precio = precio;
			}

			//Metodos get & set para obtener valor de atributo y para cambiar

			public String getMatricula() {
				return matricula;
			}



			public void setMatricula(String matricula) {
				this.matricula = matricula;
			}



			public String getMarca() {
				return marca;
			}



			public void setMarca(String marca) {
				this.marca = marca;
			}



			public String getModelo() {
				return modelo;
			}



			public void setModelo(String modelo) {
				this.modelo = modelo;
			}



			public double getPrecio() {
				return precio;
			}



			public void setPrecio(double precio) {
				this.precio = precio;
			}

			
			// Metodo toString 
			
			@Override
			public String toString() {
				return "Vehiculo [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio="
						+ precio + "]";
			}
			
		
			
}
