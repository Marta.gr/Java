
public class Moto1 {
	//Matricula, marca,	Modelo, Precio, Cilindrada, NumeroLuces, radio

	//atributos de moto
	
		//Variables con su tipo
		String matricula;
		String marca;
		String modelo;
		double precio;;
		int cilindrada; 
		int numeroLuces;
		boolean radio;
		
		//Metodo constructor para crear objetos moto
		public Moto1(String matricula, String marca, String modelo, double precio, int cilindrada, int numeroLuces,
				boolean radio) {
			super();
			this.matricula = matricula;
			this.marca = marca;
			this.modelo = modelo;
			this.precio = precio;
			this.cilindrada = cilindrada;
			this.numeroLuces = numeroLuces;
			this.radio = radio;
		}

		//Metodos get & set para obtener valor de atributo y para cambiar
		
		public String getMatricula() {
			return matricula;
		}


		public void setMatricula(String matricula) {
			this.matricula = matricula;
		}


		public String getMarca() {
			return marca;
		}


		public void setMarca(String marca) {
			this.marca = marca;
		}


		public String getModelo() {
			return modelo;
		}


		public void setModelo(String modelo) {
			this.modelo = modelo;
		}


		public double getPrecio() {
			return precio;
		}


		public void setPrecio(double precio) {
			this.precio = precio;
		}


		public double getCilindrada() {
			return cilindrada;
		}


		public void setCilindrada(int cilindrada) {
			this.cilindrada = cilindrada;
		}


		public int getNumeroLuces() {
			return numeroLuces;
		}


		public void setNumeroLuces(int numeroLuces) {
			this.numeroLuces = numeroLuces;
		}


		public boolean isRadio() {
			return radio;
		}


		public void setRadio(boolean radio) {
			this.radio = radio;
		}

		
		
		// Metodo toString 

		@Override
		public String toString() {
			return "Moto [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
					+ ", cilindrada=" + cilindrada + ", numeroLuces=" + numeroLuces + ", radio=" + radio + "]";
		}
		
				
}
