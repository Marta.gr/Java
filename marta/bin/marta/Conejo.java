
public class Conejo extends Animal{
	
	// tamaño, pelo, campero
	
	char tamaño;
	String pelo;
	boolean campero;
	
	//Metodo constructor para crear objetos Conejo

	public Conejo(String nombre, int edad, boolean chip, double peso, String color, char tamaño, String pelo,
			boolean campero) {
		super(nombre, edad, chip, peso, color);
		this.tamaño = tamaño;
		this.pelo = pelo;
		this.campero = campero;
	}

	//Metodos get & set para obtener valor de atributo y para cambiar

	public char getTamaño() {
		return tamaño;
	}


	public void setTamaño(char tamaño) {
		this.tamaño = tamaño;
	}


	public String getPelo() {
		return pelo;
	}


	public void setPelo(String pelo) {
		this.pelo = pelo;
	}


	public boolean isCampero() {
		return campero;
	}


	public void setCampero(boolean campero) {
		this.campero = campero;
	}

	
	// Metodo toString 

	@Override
	public String toString() {
		return "Conejo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", tamaño=" + tamaño + ", pelo=" + pelo + ", campero=" + campero + "]";
	}
	

	//Creacion de un metodo
	// metodo conejo tamaño <20cm y campero es comestible

		public String comestible() {
		String resultado;
			if(tamaño >= 20 && campero == true) {
				resultado = "Es comestible ";
			}else {
				resultado = "No es comestible";
			  	  }
			return resultado;
			}
	
	
	
}
