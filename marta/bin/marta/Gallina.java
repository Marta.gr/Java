
public class Gallina extends Animal {
	
	//atributos de la clase Gallina
		int numHuevos;
		boolean tieneCresta;

		
		//Metodo constructor para crear objetos Perro
		
		public Gallina(String nombre, int edad, boolean chip, double peso, String color, int numHuevos,
				boolean tieneCresta) {
			super(nombre, edad, chip, peso, color);
			this.numHuevos = numHuevos;
			this.tieneCresta = tieneCresta;
		}



		//Metodos get & set para obtener valor de atributo y para cambiar

		public int getNumHuevos() {
			return numHuevos;
		}


		public void setNumHuevos(int numHuevos) {
			this.numHuevos = numHuevos;
		}


		public boolean isTieneCresta() {
			return tieneCresta;
		}


		public void setTieneCresta(boolean tieneCresta) {
			this.tieneCresta = tieneCresta;
		}

		
		

		// Metodo toString 
	

		@Override
		public String toString() {
			return "Gallina [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color="
					+ color + ", numHuevos=" + numHuevos + ", tieneCresta=" + tieneCresta + "]";
		}
		
		
	//Creacion de un metodo
		//metodo gallina, nºhuevos /12 el nº docetas tiene

		public int nºHuevos() {
			int resultado;
			
			resultado = numHuevos/12 ;
		return resultado;
			
			}
		
		
		}