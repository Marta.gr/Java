
public class Equipo {
 //nombre, telefono, entrenador, logros_cumplidos, precio_jugar, nombre_liga
	
	String nombre;
	String entrenador;
	int telefono;
	boolean logros_cumplidos; 
	double precio_jugar;
	char nombre_liga; // 'S' liga S, 'M' liga M
	
	//Metodo Contructor

	public Equipo(String nombre, String entrenador, int telefono, boolean logros_cumplidos, double precio_jugar,
			char clasificado) {
		super();
		this.nombre = nombre;
		this.entrenador = entrenador;
		this.telefono = telefono;
		this.logros_cumplidos = logros_cumplidos;
		this.precio_jugar = precio_jugar;
		this.nombre_liga = clasificado;
	}

	//Metodos getters & setters

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEntrenador() {
		return entrenador;
	}

	public void setEntrenador(String entrenador) {
		this.entrenador = entrenador;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public boolean isLogros_cumplidos() {
		return logros_cumplidos;
	}

	public void setLogros_cumplidos(boolean logros_cumplidos) {
		this.logros_cumplidos = logros_cumplidos;
	}

	public double getPrecio_jugar() {
		return precio_jugar;
	}

	public void setPrecio_jugar(double precio_jugar) {
		this.precio_jugar = precio_jugar;
	}

	public char getnombre_liga() {
		return nombre_liga;
	}

	public void setnombre_liga(char nombre_liga) {
		this.nombre_liga = nombre_liga;
	}
	
	
	//Metodo toString para sacar el objeto por pantalla

	@Override
	public String toString() {
		return "Equipo [nombre=" + nombre + ", entrenador=" + entrenador + ", telefono=" + telefono
				+ ", logros_cumplidos=" + logros_cumplidos + ", precio_jugar=" + precio_jugar + ", nombre_liga="
				+ nombre_liga + "]";
	}
	
	
	
	
}

