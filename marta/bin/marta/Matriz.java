
public class Matriz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[][] matriz = { { 1, 3, 0, -2, 7 }, 
						   { 4, 9, 9,  5, 6 }, 
						   { 8, 1, 3,  4, 5 } };

		// Primer for recorre filas
		for (int i = 0; i < matriz.length; i++) {
			
			// Segundo for recorre columnas
			for (int j = 0; j < matriz[i].length; j++) {
				System.out.print(matriz[i][j]+"\t");
			}
			System.out.println();
		}

	}

}
