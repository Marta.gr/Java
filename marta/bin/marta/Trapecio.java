
public class Trapecio extends Figura{
	
	//atributos de la clase Trapecio
		// bMayor, bMenor, altura
	
	double bMayor;
	double bMenor;
	double altura;
	
	//Metodo constructor para crear objetos Trapecio

	public Trapecio(String color, char tamaño, double bMayor, double bMenor, double altura) {
		super(color, tamaño);
		this.bMayor = bMayor;
		this.bMenor = bMenor;
		this.altura = altura;
	}
	
	//Metodos get & set para obtener valor de atributo y para cambiar


	public double getbMayor() {
		return bMayor;
	}

	public void setbMayor(double bMayor) {
		this.bMayor = bMayor;
	}

	public double getbMenor() {
		return bMenor;
	}

	public void setbMenor(double bMenor) {
		this.bMenor = bMenor;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	
	// Metodo toString 

	@Override
	public String toString() {
		return "Trapecio [color=" + color + ", tamaño=" + tamaño + ", bMayor=" + bMayor + ", bMenor=" + bMenor
				+ ", altura=" + altura + "]";
	}
	
	
	//Metodo calcularArea

		public double calcularArea() {
		
			
			return ((bMayor+bMenor)/2)*altura;
			
		}
}
