
public class Rombo extends Figura {
	
	//atributos de la clase Rombo
		// dMAyor, dMenor

	double dMayor;
	double dMenor;
	
	//Metodo constructor para crear objetos Rombo

	public Rombo(String color, char tamaño, double dMayor, double dMenor) {
		super(color, tamaño);
		this.dMayor = dMayor;
		this.dMenor = dMenor;
	}
	
	//Metodos get & set para obtener valor de atributo y para cambiar

	public double getdMayor() {
		return dMayor;
	}

	public void setdMayor(double dMayor) {
		this.dMayor = dMayor;
	}

	public double getdMenor() {
		return dMenor;
	}

	public void setdMenor(double dMenor) {
		this.dMenor = dMenor;
	}

	// Metodo toString 

	@Override
	public String toString() {
		return "Rombo [color=" + color + ", tamaño=" + tamaño + ", dMayor=" + dMayor + ", dMenor=" + dMenor + "]";
	}
	
	//Metodo calcularArea

		public double calcularArea() {
		
			
			return (dMayor*dMenor)/2;
			
		}
	
}
