
public class Triangulo extends Figura {

	//atributos de la clase Triiangulo
		// base, altura
	
	double base;
	double altura;
	
	//Metodo constructor para crear objetos Triangulo

	public Triangulo(String color, char tamaño, double base, double altura) {
		super(color, tamaño);
		this.base = base;
		this.altura = altura;
	}
	
	
	//Metodos get & set para obtener valor de atributo y para cambiar

	public double getBase() {
		return base;
	}

	public void setBase(double base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	// Metodo toString 

	@Override
	public String toString() {
		return "Triangulo [color=" + color + ", tamaño=" + tamaño + ", base=" + base + ", altura=" + altura + "]";
	}
	
	//Metodo calcularArea

		public double calcularArea() {
			
			return (base * altura)/2;
			
		}
	
}
