
public class Coche extends Vehiculo {
	
//atributos de la clase coche
	char tipo; // 'G' gasolina, 'D' diesel, 'H' hibrido, 'E' electrico, 'O' otro
	int bastidor;
	int rueda;
	boolean descapotable;
	
	
	//Metodo constructor para crear objetos Coche

	public Coche(String matricula, String marca, String modelo, double precio, char tipo, int bastidor, int rueda,
			boolean descapotable) {
		super(matricula, marca, modelo, precio);
		this.tipo = tipo;
		this.bastidor = bastidor;
		this.rueda = rueda;
		this.descapotable = descapotable;
	}

	
	//Metodos get & set para obtener valor de atributo y para cambiar

	public char getTipo() {
		return tipo;
	}



	public void setTipo(char tipo) {
		this.tipo = tipo;
	}



	public int getBastidor() {
		return bastidor;
	}



	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}



	public int getRueda() {
		return rueda;
	}



	public void setRueda(int rueda) {
		this.rueda = rueda;
	}



	public boolean isDescapotable() {
		return descapotable;
	}



	public void setDescapotable(boolean descapotable) {
		this.descapotable = descapotable;
	}


	
	// Metodo toString 

	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", tipo=" + tipo + ", bastidor=" + bastidor + ", rueda=" + rueda + ", descapotable=" + descapotable
				+ "]";
	}
	
	

}
