
public class Moto extends Vehiculo {
	
	//atributos de la clase Moto
	int cilindrada; 
	int numeroLuces;
	boolean radio;
	
	
	//Metodo constructor para crear objetos Moto

	public Moto(String matricula, String marca, String modelo, double precio, int cilindrada, int numeroLuces,
			boolean radio) {
		super(matricula, marca, modelo, precio);
		this.cilindrada = cilindrada;
		this.numeroLuces = numeroLuces;
		this.radio = radio;
	}

	//Metodos get & set para obtener valor de atributo y para cambiar

	public int getCilindrada() {
		return cilindrada;
	}


	public void setCilindrada(int cilindrada) {
		this.cilindrada = cilindrada;
	}


	public int getNumeroLuces() {
		return numeroLuces;
	}


	public void setNumeroLuces(int numeroLuces) {
		this.numeroLuces = numeroLuces;
	}


	public boolean isRadio() {
		return radio;
	}


	public void setRadio(boolean radio) {
		this.radio = radio;
	}

	
	// Metodo toString 

	@Override
	public String toString() {
		return "Moto [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", cilindrada=" + cilindrada + ", numeroLuces=" + numeroLuces + ", radio=" + radio + "]";
	}
	
}
