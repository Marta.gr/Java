
public class Coche1 {
	//Matricula, marca,	Modelo, Precio, Tipo, Bastidor, Rueda, Descapotable

	//Variables con su tipo
	String matricula;
	String marca;
	String modelo;
	double precio;;
	char tipo; // 'G' gasolina, 'D' diesel, 'H' hibrido, 'E' electrico, 'O' otro
	int bastidor;
	int rueda;
	boolean descapotable;
	
	//Metodo Contructor
	
	public Coche1(String matricula, String marca, String modelo, double precio, char tipo, int bastidor, int rueda,
			boolean descapotable) {
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.tipo = tipo;
		this.bastidor = bastidor;
		this.rueda = rueda;
		this.descapotable = descapotable;
	}

	//Metodos getters & setters
	
	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public char getTipo() {
		return tipo;
	}


	public void setTipo(char tipo) {
		this.tipo = tipo;
	}


	public int getBastidor() {
		return bastidor;
	}


	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}


	public int getRueda() {
		return rueda;
	}


	public void setRueda(int rueda) {
		this.rueda = rueda;
	}


	public boolean isDescapotable() {
		return descapotable;
	}


	public void setDescapotable(boolean descapotable) {
		this.descapotable = descapotable;
	}

	//Metodo toString para sacar el objeto por pantalla
	
	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", tipo=" + tipo + ", bastidor=" + bastidor + ", rueda=" + rueda + ", descapotable=" + descapotable
				+ "]";
	}
	
	
//Creacion de un método
	
	//Calcular Precio con IVA
	
	public double calcularIVA() {
	return precio*1.21;
	}
	
	//Obtener categoria del coche
	public char categoria() {
		char cat=' ';
		if (tipo == 'D') cat ='B';
		if (tipo == 'G') cat ='C';
		if (tipo == 'H') cat ='A';
	return cat;		
	}
	
	//Se ha pinchado una rueda, resta una rueda a las que tiene
	public void pincharRueda() {
		rueda = getRueda() -1;
	}
		
}
