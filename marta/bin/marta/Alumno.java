
public class Alumno {
 //nombre, Apellidoss, DNI, telefono_movil, sexo, media, pasa_curso
	
	//Variables con su tipo
	
	String nombre;
	String apellidos;
	String DNI;
	int telefono_movil;
	char sexo; //'M' Masculino, 'F' Femenino
	double media;
	boolean pasa_curso; // 'S' si pasa, 'N' no pasa+
	
	// Metodo Constructo

	
	public Alumno(String nombre, String apellidos, String dNI, int telefono_movil, char sexo, double media,
			boolean pasa_curso) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		DNI = dNI;
		this.telefono_movil = telefono_movil;
		this.sexo = sexo;
		this.media = media;
		this.pasa_curso = pasa_curso;
	}

	
	//Metodos getters & setters
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public int getTelefono_movil() {
		return telefono_movil;
	}

	public void setTelefono_movil(int telefono_movil) {
		this.telefono_movil = telefono_movil;
	}

	public char getSexo() {
		return sexo;
	}

	public void setSexo(char sexo) {
		this.sexo = sexo;
	}

	public double getMedia() {
		return media;
	}

	public void setMedia(double media) {
		this.media = media;
	}

	public boolean isPasa_curso() {
		return pasa_curso;
	}

	public void setPasa_curso(boolean pasa_curso) {
		this.pasa_curso = pasa_curso;
	}


	
	//Metodo toString para sacar el objeto por pantalla

	@Override
	public String toString() {
		return "Alumno [nombre=" + 	//Metodo toString para sacar el objeto por pantalla
nombre + ", apellidos=" + apellidos + ", DNI=" + DNI + ", telefono_movil="
				+ telefono_movil + ", sexo=" + sexo + ", media=" + media + ", pasa_curso=" + pasa_curso + "]";
	}
	
	
	
}
