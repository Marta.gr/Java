
public class Figura {
// Color, tamaño
	
	//atributos de la clase Figura (padre)
	
	String color;
	char tamaño;
	
	//Metodo constructor para crear objetos vehiculo
	
	public Figura(String color, char tamaño) {
		super();
		this.color = color;
		this.tamaño = tamaño;
	}

	
	//Metodos get & set para obtener valor de atributo y para cambiar

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public char getTamaño() {
		return tamaño;
	}

	public void setTamaño(char tamaño) {
		this.tamaño = tamaño;
	}

	
	// Metodo toString 
	
	@Override
	public String toString() {
		return "Figura [color=" + color + ", tamaño=" + tamaño + "]";
	}

	
}
