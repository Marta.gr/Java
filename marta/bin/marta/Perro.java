
public class Perro extends Animal {
	
	//atributos de la clase Perro
	String raza;
	Boolean ladra;
	char tipo;
	
	//Metodo constructor para crear objetos Perro

	
	public Perro(String nombre, int edad, boolean chip, double peso, String color, String raza, Boolean ladra,
			char tipo) {
		super(nombre, edad, chip, peso, color);
		this.raza = raza;
		this.ladra = ladra;
		this.tipo = tipo;
	}
	

	//Metodos get & set para obtener valor de atributo y para cambiar

	public String getRaza() {
		return raza;
	}



	public void setRaza(String raza) {
		this.raza = raza;
	}



	public Boolean getLadra() {
		return ladra;
	}



	public void setLadra(Boolean ladra) {
		this.ladra = ladra;
	}



	public char getTipo() {
		return tipo;
	}



	public void setTipo(char tipo) {
		this.tipo = tipo;
	}


	// Metodo toString 

	@Override
	public String toString() {
		return "Perro [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", raza=" + raza + ", ladra=" + ladra + ", tipo=" + tipo + "]";
	}
	
//Creacion de un metodo
	// si ladra y es mayor, tiene ladrido mas agudo

	public String mayla() {
	String resultado;
		if(ladra == true && edad >= 4) {
			resultado = "Tiene el ladrido grave";
		}else {
			resultado = "Tiene el ladrido agudo";
		  	  }
		return resultado;
		}
}
