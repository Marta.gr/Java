
public class Caballo extends Animal {
	
//atributos de la clase Animal
	// altura, salvaje, sexo
	
	double altura;
	boolean esSalvaje;
	char sexo;
	
	
	//Metodo constructor para crear objetos Caballo

	
	public Caballo(String nombre, int edad, boolean chip, double peso, String color, double altura, boolean esSalvaje,
			char sexo) {
		super(nombre, edad, chip, peso, color);
		this.altura = altura;
		this.esSalvaje = esSalvaje;
		this.sexo = sexo;
	}


	
	//Metodos get & set para obtener valor de atributo y para cambiar

	
	public double getAltura() {
		return altura;
	}


	public void setAltura(double altura) {
		this.altura = altura;
	}


	public boolean isEsSalvaje() {
		return esSalvaje;
	}


	public void setEsSalvaje(boolean esSalvaje) {
		this.esSalvaje = esSalvaje;
	}


	public char getSexo() {
		return sexo;
	}


	public void setSexo(char sexo) {
		this.sexo = sexo;
	}


	// Metodo toString 


	@Override
	public String toString() {
		return "Caballo [nombre=" + nombre + ", edad=" + edad + ", chip=" + chip + ", peso=" + peso + ", color=" + color
				+ ", altura=" + altura + ", esSalvaje=" + esSalvaje + ", sexo=" + sexo + "]";
	}
	
	//Creacion de un metodo
		// si es salvaje no se puede montar 

		public String montar() {
			String resultado = null ;
		
			if(esSalvaje == true ) {
				resultado = "No se puede montar";
			}
			return resultado;
			}
	
	
}
