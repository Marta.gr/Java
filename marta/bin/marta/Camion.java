
public class Camion extends Vehiculo{

	//atributos de Camion
			//Variables con su tipo
	double peso; 
	int nºEjes;
	boolean remolque;
	double capacidad;
	char señal; //lo que transporte
	
	
	//Metodo constructor para crear objetos Camion
	
	public Camion(String matricula, String marca, String modelo, double precio, double peso, int nºEjes,
			boolean remolque, double capacidad, char señal) {
		super(matricula, marca, modelo, precio);
		this.peso = peso;
		this.nºEjes = nºEjes;
		this.remolque = remolque;
		this.capacidad = capacidad;
		this.señal = señal;
	}

	//Metodos get & set para contener valores de los atributos o cambiarlos

	public double getPeso() {
		return peso;
	}


	public void setPeso(double peso) {
		this.peso = peso;
	}


	public int getNºEjes() {
		return nºEjes;
	}


	public void setNºEjes(int nºEjes) {
		this.nºEjes = nºEjes;
	}


	public boolean isRemolque() {
		return remolque;
	}


	public void setRemolque(boolean remolque) {
		this.remolque = remolque;
	}


	public double getCapacidad() {
		return capacidad;
	}


	public void setCapacidad(double capacidad) {
		this.capacidad = capacidad;
	}


	public char getSeñal() {
		return señal;
	}


	public void setSeñal(char señal) {
		this.señal = señal;
	}

	
	// Metodo toString 

	@Override
	public String toString() {
		return "Camion [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", peso=" + peso + ", nºEjes=" + nºEjes + ", remolque=" + remolque + ", capacidad=" + capacidad
				+ ", señal=" + señal + "]";
	}
	
	
	
	
	
}
