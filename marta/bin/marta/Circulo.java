
public class Circulo extends Figura{
	
//atributos de la clase Circulo
	// radio
	
	double radio;

	//Metodo constructor para crear objetos Circulo

	
public Circulo(String color, char tamaño, double radio) {
	super(color, tamaño);
	this.radio = radio;
}


//Metodos get & set para obtener valor de atributo y para cambiar


public double getRadio() {
	return radio;
}



public void setRadio(double radio) {
	this.radio = radio;
}


// Metodo toString 

@Override
public String toString() {
	return "Circulo [color=" + color + ", tamaño=" + tamaño + ", radio=" + radio + "]";
}

//Metodo calcularArea

	public double calcularArea() {
	
		double PI = 3.14;
		
		return radio * PI;
		
	}


}
