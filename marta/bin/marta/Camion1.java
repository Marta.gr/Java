
public class Camion1 { 
	
	//Matricula, marca,	Modelo, Precio, Peso, nºEjes, Remolque, Capacidad, señal

	//atributos de Camion
		//Variables con su tipo
		String matricula;
		String marca;
		String modelo;
		double precio;;
		double peso; 
		int nºEjes;
		boolean remolque;
		double capacidad;
		char señal; //lo que transporte
		
		
		//Metodo constructor para crear objetos Camion
		
		public Camion1(String matricula, String marca, String modelo, double precio, double peso, int nºEjes,
				boolean remolque, double capacidad, char señal) {
			super();
			this.matricula = matricula;
			this.marca = marca;
			this.modelo = modelo;
			this.precio = precio;
			this.peso = peso;
			this.nºEjes = nºEjes;
			this.remolque = remolque;
			this.capacidad = capacidad;
			this.señal = señal;
		}


//Metodos get & set para contener valores de los atributos o cambiarlos

		public String getMatricula() {
			return matricula;
		}




		public void setMatricula(String matricula) {
			this.matricula = matricula;
		}




		public String getMarca() {
			return marca;
		}




		public void setMarca(String marca) {
			this.marca = marca;
		}




		public String getModelo() {
			return modelo;
		}




		public void setModelo(String modelo) {
			this.modelo = modelo;
		}




		public double getPrecio() {
			return precio;
		}




		public void setPrecio(double precio) {
			this.precio = precio;
		}




		public double getPeso() {
			return peso;
		}




		public void setPeso(double peso) {
			this.peso = peso;
		}




		public int getNºEjes() {
			return nºEjes;
		}




		public void setNºEjes(int nºEjes) {
			this.nºEjes = nºEjes;
		}




		public boolean isRemolque() {
			return remolque;
		}




		public void setRemolque(boolean remolque) {
			this.remolque = remolque;
		}




		public double getCapacidad() {
			return capacidad;
		}




		public void setCapacidad(double capacidad) {
			this.capacidad = capacidad;
		}




		public char getSeñal() {
			return señal;
		}




		public void setSeñal(char señal) {
			this.señal = señal;
		}



		// Metodo toString 

		@Override
		public String toString() {
			return "Camion [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
					+ ", peso=" + peso + ", nºEjes=" + nºEjes + ", remolque=" + remolque + ", capacidad=" + capacidad
					+ ", señal=" + señal + "]";
		}
		
		
		
}
